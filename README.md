# API DE VENDAS 💲

## Sobre 🧡

O projeto **API DE VENDAS** é uma api Rest desenvolvida ao final do Bootcamp da **DIO** em parceria com a **Pottencial** como teste tecnológico.

---

## Apresentação da estrutura do projeto 🧡
![Estrutura do Projeto](src/assets/videoapresentacao1.mp4)

---

## Apresentação Funcionando no Swagger 🧡 
![Swagger](src/assets/videoapresentacao2.mp4)

---

## Tecnologias Utilizadas

- C#
- .Net


---

## Minha Solução

- Criei as entidades que representam as tabelas no banco de dados;
- Camada de persistência onde ficou o repositório; 
- Controllers que assumem a responsabilidade de receber e transmitir informações ao usuário da api; 
- Services com a responsabilidade de contactar o banco de dados e garantir as regras de negócio; 
- Classes DTOs para trafegar na rede; 
- Classe de validação para auxiliar a service na validação do status;
- Criei um endpoint para cadastrar os vendedores a fim de que a cada venda cadastrada seja possível acrescentar um vendedor pelo seu ID;
- Criei um endpoint (POST) para cadastrar as vendas, um endpoint (GET) para buscar a venda por ID e um endpoint (PATCH) para modificar o status.

---

### Mensagem à Pottencial
**#EuQueroSerPotter** 🧡🧡🧡

Obrigada pela oportunidade!





