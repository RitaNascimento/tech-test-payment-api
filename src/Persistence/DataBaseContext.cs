using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Entities;

namespace tech_test_payment_api.src.Persistence;

public class DataBaseContext : DbContext
{

    public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
    {

    }
    public DbSet<Venda> Vendas { get; set; }

    public DbSet<Produto> Itens { get; set; }

    public DbSet<Vendedor> Vendedores { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Venda>(tabela =>
        {
            tabela.HasKey(e => e.Id);
            tabela
                .HasMany(e => e.Itens)
                .WithOne()
                .HasForeignKey(p => p.VendaId);
            tabela.HasOne(e => e.vendedor);
        });

        builder.Entity<Vendedor>(tabela =>
        {
            tabela.HasKey(e => e.Id);
        });

        builder.Entity<Produto>(tabela =>
        {
            tabela.HasKey(e => e.Id);
        });

    }
}
