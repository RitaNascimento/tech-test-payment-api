using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Dto;

namespace tech_test_payment_api.src.Entities
{
    public class Produto
    {
        public Produto()
        {
        
        }
        public Produto(int id, string nome, decimal preco, int vendaId) 
        {
            this.Id = id;
            this.Nome = nome;
            this.Preco = preco;
            this.VendaId = vendaId;
        }

        public Produto(ProdutoDTO  dto)
        {
            this.Id = dto.Id;
            this.Nome = dto.Nome;
            this.Preco = dto.Preco;
            this.VendaId = dto.VendaId;
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int VendaId { get; set; }
    }
}