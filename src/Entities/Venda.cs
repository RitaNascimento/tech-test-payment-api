using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Entities
{
    public class Venda
    {
        public Venda()
        {
            this.DataVenda = DateTime.Now;
            this.Itens = new List<Produto>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }
        public Venda(int id, string identificadorDoPedido, Vendedor vendedor) 
        {
            this.Id = id;
            this.IdentificadorDoPedido = identificadorDoPedido;
            this.vendedor = vendedor;
            this.DataVenda = DateTime.Now;
            this.Itens = new List<Produto>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public string IdentificadorDoPedido { get; set; }
        public Vendedor vendedor { get; set; }
        public StatusVenda statusVenda { get; set; }
        public List<Produto> Itens { get; set; }

    }
}