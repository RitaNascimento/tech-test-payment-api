namespace tech_test_payment_api.src.Enums
{
    public enum StatusVenda 
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada

    }
}