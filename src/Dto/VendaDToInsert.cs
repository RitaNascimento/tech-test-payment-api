using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Entities;
using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Dto
{
    public class VendaDToInsert
    {
        public VendaDToInsert()
        {
            this.DataVenda = DateTime.Now;
            this.Itens = new List<ProdutoDTO>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }
        public VendaDToInsert(int id, string identificadorDoPedido, int vendedorId) 
        {
            this.Id = id;
            this.IdentificadorDoPedido = identificadorDoPedido;
            this.vendedorId = vendedorId;
            this.DataVenda = DateTime.Now;
            this.Itens = new List<ProdutoDTO>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }

        public VendaDToInsert(Venda venda) 
        {
            this.Id = venda.Id;
            this.IdentificadorDoPedido = venda.IdentificadorDoPedido;
            this.vendedorId = venda.vendedor.Id;
            this.DataVenda = DateTime.Now;
            this.statusVenda = venda.statusVenda;
            this.Itens = new List<ProdutoDTO>();
            venda.Itens.ForEach(x => this.Itens.Add(new ProdutoDTO(x)));
        }
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public string IdentificadorDoPedido { get; set; }
        public int vendedorId { get; set; }
        public StatusVenda statusVenda { get; set; }
        public List<ProdutoDTO> Itens { get; set; }
    }
}