using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Entities;

namespace tech_test_payment_api.src.Dto
{
    public class VendedorDTO
    {
        public VendedorDTO()
        {
            
        }
        public VendedorDTO(int id, string nome, string cpf , string email, string telefone) 
        {
            this.Id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.Email = email;
            this.Telefone = telefone;
        }

        public VendedorDTO(Vendedor vendedor)
        {
            this.Id = vendedor.Id;
            this.Nome = vendedor.Nome;
            this.Cpf = vendedor.Cpf;
            this.Email = vendedor.Email;
            this.Telefone = vendedor.Telefone;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}