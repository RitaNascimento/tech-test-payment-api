using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Entities;

namespace tech_test_payment_api.src.Dto
{
    public class ProdutoDTO
    {
        
        public ProdutoDTO()
        {
        
        }
        public ProdutoDTO(int id, string nome, decimal preco, int vendaId) 
        {
            this.Id = id;
            this.Nome = nome;
            this.Preco = preco;
            this.VendaId = vendaId;
        }

        public ProdutoDTO(Produto produto)
        {
            this.Id = produto.Id;
            this.Nome = produto.Nome;
            this.Preco = produto.Preco;
            this.VendaId = produto.VendaId;
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int VendaId { get; set; }
    }
}