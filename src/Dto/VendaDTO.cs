using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Entities;
using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Dto
{
    public class VendaDTO
    {
        public VendaDTO()
        {
            this.DataVenda = DateTime.Now;
            this.Itens = new List<ProdutoDTO>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }
        public VendaDTO(int id, string identificadorDoPedido, VendedorDTO vendedorDTO) 
        {
            this.Id = id;
            this.IdentificadorDoPedido = identificadorDoPedido;
            this.vendedor = vendedorDTO;
            this.DataVenda = DateTime.Now;
            this.Itens = new List<ProdutoDTO>();
            this.statusVenda = StatusVenda.AguardandoPagamento;
        }

        public VendaDTO(Venda venda) 
        {
            this.Id = venda.Id;
            this.IdentificadorDoPedido = venda.IdentificadorDoPedido;
            this.vendedor = new VendedorDTO(venda.vendedor);
            this.DataVenda = DateTime.Now;
            this.statusVenda = venda.statusVenda;
            this.Itens = new List<ProdutoDTO>();
            venda.Itens.ForEach(x => this.Itens.Add(new ProdutoDTO(x)));
        }
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public string IdentificadorDoPedido { get; set; }
        public VendedorDTO vendedor { get; set; }
        public StatusVenda statusVenda { get; set; }
        public List<ProdutoDTO> Itens { get; set; }
    }
}