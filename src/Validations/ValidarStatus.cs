using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Validations
{
    public class ValidarStatus
    {
        public static Boolean StatusVendaValidado(StatusVenda statusAtual, StatusVenda statusNovo)
        {
            bool status = statusAtual == StatusVenda.AguardandoPagamento && 
            (statusNovo == StatusVenda.PagamentoAprovado || statusNovo == StatusVenda.Cancelada);

            bool status1 = statusAtual == StatusVenda.PagamentoAprovado &&
            (statusNovo == StatusVenda.EnviadoParaTransportadora || statusNovo == StatusVenda.Cancelada);

            bool status2 = statusAtual == StatusVenda.EnviadoParaTransportadora &&
            statusNovo == StatusVenda.Entregue;

            if(status || status1 || status2) return true;

            return false;
        }
    }
}