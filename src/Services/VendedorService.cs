using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Dto;
using tech_test_payment_api.src.Entities;
using tech_test_payment_api.src.Persistence;

namespace tech_test_payment_api.src.Services
{
    public class VendedorService
    {
        private DataBaseContext _repository { get; set; }

        public VendedorService(DataBaseContext repository)
        {
            this._repository = repository;
        }

         public (bool sucesso, string mensagem) CriarVendedor(VendedorDTO vendedorDTO)
        {
            Vendedor vendedor = new Vendedor();
            var vendedorExiste = _repository.Vendedores.Any(v => v.Cpf.ToUpper() == vendedorDTO.Cpf.ToUpper());
            if (vendedorExiste) return (false, "Já existe um vendedor cadastrado com este CPF");

            vendedor.Nome = vendedorDTO.Nome;
            vendedor.Cpf = vendedorDTO.Cpf;
            vendedor.Email = vendedorDTO.Email;
            vendedor.Telefone = vendedorDTO.Telefone;

            _repository.Vendedores.Add(vendedor);
            _repository.SaveChanges();
            var Dto = new VendedorDTO(vendedor);
            return (true, $"Vendedor cadastrado com sucesso com id = {Dto.Id}");
        }


    }
}