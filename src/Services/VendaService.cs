using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Dto;
using tech_test_payment_api.src.Entities;
using tech_test_payment_api.src.Enums;
using tech_test_payment_api.src.Persistence;
using tech_test_payment_api.src.Validations;

namespace tech_test_payment_api.src.Services
{
    
    public class VendaService
    {
        private DataBaseContext _repository { get; set; }

        public VendaService(DataBaseContext repository)
        {
            this._repository = repository;
        }

         public (bool sucesso, string mensagem, VendaDTO) RegistrarVenda(VendaDToInsert vendaDToInsert)
        {
            var verificaVendedor = _repository.Vendedores.Find(vendaDToInsert.vendedorId);
            var itemVenda = vendaDToInsert.Itens.Any();
            
            if(verificaVendedor==null) 
                return (false, "É necessário definir um id válido de um vendedor para registrar uma venda", null);
            if (!itemVenda) return (false, "Uma venda deverá possuir pelo menos um ítem", null);
            
            vendaDToInsert.statusVenda = StatusVenda.AguardandoPagamento;
            Venda venda = new Venda();
            copiarVendaDToInsertParaVenda(vendaDToInsert, venda);
            _repository.Vendas.Add(venda);
            _repository.SaveChanges();

            return (true, $"Venda cadastrada com sucesso com id = {venda.Id}", new VendaDTO(venda));
        }

        public (bool sucesso, string mensagem, VendaDTO vendaDTO) BuscarVendaPorId(int id)
        {
            var venda = _repository.Vendas
                                    .Include(v => v.Itens)
                                    .Include(v => v.vendedor).SingleOrDefault(v => v.Id == id);
            if (venda==null) return (false, $"A venda de id = {id} não existe", null);

            return (true, "", new VendaDTO(venda));
        }

        public (bool sucesso, string mensagem, VendaDTO vendaDTOAtualizada) AtualizarStatusVenda(int id, VendaDTO novaVendaDTO)
        {
            var (existeVenda, msg, vendaDTO) = BuscarVendaPorId(id);

             var statusAtual = vendaDTO.statusVenda;
            var novoStatus = novaVendaDTO.statusVenda;

            bool statusValidado = ValidarStatus.StatusVendaValidado(statusAtual, novoStatus);
            if(!statusValidado) return (false, $"A modificação do status {statusAtual} para {novoStatus} não é permitida", null);
            
            Venda venda = _repository.Vendas.Find(vendaDTO.Id);
            venda.statusVenda = novaVendaDTO.statusVenda;

            _repository.Vendas.Update(venda);
            _repository.SaveChanges();

            return (true, $"o status da venda de id = {id} foi modificado para {novaVendaDTO.statusVenda} com sucesso", new VendaDTO(venda));
        }

        private void copiarVendaDToInsertParaVenda (VendaDToInsert vendaDToInsert, Venda venda)
        {
            venda.Id = vendaDToInsert.Id;
            venda.IdentificadorDoPedido = vendaDToInsert.IdentificadorDoPedido;
            venda.statusVenda = vendaDToInsert.statusVenda;
            venda.vendedor = _repository.Vendedores.Find(vendaDToInsert.vendedorId);
            vendaDToInsert.Itens.ForEach(x => venda.Itens.Add(new Produto(x)));

        }

    }
}