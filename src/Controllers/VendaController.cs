using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Persistence;
using tech_test_payment_api.src.Entities;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Enums;
using tech_test_payment_api.src.Validations;
using tech_test_payment_api.src.Services;
using tech_test_payment_api.src.Dto;

namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]")]
    public class VendaController : ControllerBase
    {
        private VendaService _service { get; set; }
        

        public VendaController (VendaService service)
        {
            this._service = service;
        }

        [HttpPost]
        public IActionResult Registrar(VendaDToInsert vendaDToInsert)
        {
           var (sucesso, mensagem, vendaDTO) = _service.RegistrarVenda(vendaDToInsert);
           if(!sucesso) 
           return BadRequest(new { Erro = mensagem });
           
           return Created(mensagem, vendaDTO);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVendaPorId(int id)
        { 
            var (sucesso, mensagem, vendaDTO) = _service.BuscarVendaPorId(id);
            if(!sucesso) return NotFound(new { Erro = mensagem });

            return Ok(vendaDTO);
        }

        [HttpPatch("{id}")]
        public IActionResult AtualizarStatusVenda(int id, VendaDTO novaVendaDTO)
        {
            var (vendaExiste, msg, vendaDTO) = _service.BuscarVendaPorId(id);
            if(!vendaExiste) return NotFound(new { Erro = msg });

            var (sucesso, mensagem, vendaDTOAtualizada) = _service.AtualizarStatusVenda(id, novaVendaDTO);
            if (!sucesso) return BadRequest(new { Erro = mensagem });

            return Ok(new
            {
                msg = mensagem,
                vendaDTOAtualizada
        });
        }
    }
}