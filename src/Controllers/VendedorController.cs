using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Dto;
using tech_test_payment_api.src.Entities;
using tech_test_payment_api.src.Services;

namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]")]
    public class VendedorController : ControllerBase
    {
        private VendedorService _service { get; set; }
        

        public VendedorController (VendedorService service)
        {
            this._service = service;
        }
        
        [HttpPost]
        public IActionResult Criar(VendedorDTO vendedorDTO)
        {
           var (sucesso, mensagem) = _service.CriarVendedor(vendedorDTO);
           if(!sucesso) 
           return BadRequest(new { Erro = mensagem });
           
           return Created(mensagem, vendedorDTO);
        }
    }
}